defmodule CardsTest do
  use ExUnit.Case
  doctest Cards
  @deck ["Ace", "Two", "Three"]

  test "creates a deck of cards" do
    assert Enum.count(Cards.create_deck) == 52
  end

  test "shuffles the cards" do
    assert Cards.shuffle(@deck)
  end

  test "tells whether a deck contains a specific card" do
    assert Cards.contains?(@deck, "Ace") == true
    assert Cards.contains?(@deck, "Four") == false
  end

  test "deals the specified amount of cards from deck" do
    {hand, _} = Cards.deal(@deck, 2)
    assert Enum.count(hand) == 2
  end

  test "saves a deck" do
    
  end

  test "loads a saved deck" do
    
  end
end
